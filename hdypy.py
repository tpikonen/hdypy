import re
import sys
import tokenize
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('Handy', '1')
from gi.repository import Gio, Gtk
from gi.repository import Handy


class HApp(Gtk.Application):

    def __init__(self, *args, **kwargs):
        Gtk.Application.__init__(
            self, *args, application_id="org.example.myapp",
            flags=Gio.ApplicationFlags.FLAGS_NONE, **kwargs)
        Handy.init()

        self.builder = Gtk.Builder()
        self.builder.add_from_file("hdypy.ui")
        self.builder.connect_signals(self)
        for widget in self.builder.get_objects():
            if not isinstance(widget, Gtk.Buildable):
                continue
            # The following call looks ugly, but see Gnome bug 591085
            widget_name = Gtk.Buildable.get_name(widget)

            widget_api_name = '_'.join(re.findall(tokenize.Name, widget_name))
            if hasattr(self, widget_api_name):
                raise AttributeError(
                    "instance %s already has an attribute %s" % (
                        self, widget_api_name))
            else:
                setattr(self, widget_api_name, widget)
        self.connect("activate", self.do_activate)

    def do_activate(self, *args):
        self.add_window(self.window)
        self.window.show()
        return True

    def leaflet_forward_cb(self, button):
        self.leaflet.navigate(Handy.NavigationDirection.FORWARD)
        return True

    def leaflet_back_cb(self, button):
        self.leaflet.navigate(Handy.NavigationDirection.BACK)
        return True

    def deck_forward_cb(self, button):
        self.deck.navigate(Handy.NavigationDirection.FORWARD)
        return True

    def deck_back_cb(self, button):
        self.deck.navigate(Handy.NavigationDirection.BACK)
        return True


def main():
    app = HApp()
    app.run()
    sys.exit(0)


main()
